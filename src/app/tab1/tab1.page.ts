import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UniversityService } from '../services/university.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  username: string = 'Sako';
  color: string = 'dark';
  colorIcon: string = 'light';
  isLoad: boolean = true;
  cards: boolean = false;
  limit: number = 5;
  offset: number = 0;
  country: string = 'Albania';
  dataFromBackend: any = [];
  geoData: any = [];
  isGeoLoaded: boolean = true;

  public data: any = [];
  constructor(
    public domSanitizer: DomSanitizer,
    public universityService: UniversityService) {

  }
ngOnInit(): void {
  this.isLoad = true;
  let url = 
  `http://universities.hipolabs.com/search?country=${this.country}&limit=${this.limit}`;
  this.universityService.getData(url).subscribe((res: any)=>{
    console.log(res)
    this.data = res;
    this.isLoad = false;
  })
  this.isGeoLoaded = true;
  let geoUrl = "https://get.geojs.io/v1/ip/geo.json";
  this.universityService.getData(geoUrl).subscribe((res: any)=>{
    this.geoData = res;
    this.isGeoLoaded = false;
  })
}
getUni(uni: string = 'Albania'){
  this.country = uni;
  let url = `http://universities.hipolabs.com/search?country=${uni}&limit=${this.limit}&offset=0`;
  this.universityService.getData(url).subscribe((res: any)=>{
    console.log(res)
    this.dataFromBackend = res;
    this.data = res;
  })
}
 kliko(): any {

  if(this.color == 'danger'){
    this.color = 'dark';
    this.colorIcon = 'warning';
  }
  else {
    this.color = 'danger';
    this.colorIcon = 'light';
  }
}
showCards(){
  this.cards= true;
}
showMore(){
  this.offset++;
  let data = this.offset * this.limit;
  let url = 
  `http://universities.hipolabs.com/search?country=${this.country}&limit=${this.limit}&offset=${data}`;
  this.universityService.getData(url).subscribe((res: any)=>{
    console.log(res)
    this.dataFromBackend = res;
    this.data = [...this.data, ...res];
  })
}
geoMap(lat: string, lon: string){
  let mapUrl='https://maps.google.com/maps?q=';
return this.domSanitizer.bypassSecurityTrustResourceUrl(mapUrl+lat+','+lon+'&hl=es&z=14&amp;output=embed')
}
}
