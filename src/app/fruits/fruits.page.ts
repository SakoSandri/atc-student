import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.page.html',
  styleUrls: ['./fruits.page.scss'],
})
export class FruitsPage implements OnInit {
  public fruit1: any = [
    {
      img: "assets/fruits/apple.png",
      point: 40
    },
    {
      img: "assets/fruits/cherry.png"
    },
    {
      img: "assets/fruits/orange.png"
    }
  ];
  public fruit2: any = [
    {
      img: "assets/fruits/apple.png"
    },
    {
      img: "assets/fruits/cherry.png"
    },
    {
      img: "assets/fruits/orange.png"
    }
  ];
  public fruit3: any = [
    {
      img: "assets/fruits/apple.png"
    },
    {
      img: "assets/fruits/cherry.png"
    },
    {
      img: "assets/fruits/orange.png"
    }
  ];
  public img1: any = "";
  public img2: any = "";
  public img3: any = "";  
  public img4: any = "";

  public isWinner: any =  localStorage.getItem("isWinner") || 0;
  public youAreWinner: boolean = false;
  public counter : any = localStorage.getItem("counter") || 0;
  public ratioWinner: number = ((this.isWinner/this.counter) * 100);
  public expenseValue: number = 10;
  public winnerValue: number = 100;
  public loses: any = localStorage.getItem("loses") || 0;
  public earnings: any = localStorage.getItem("earnings") || 0;
  
  constructor() { }

  ngOnInit() {
  }
  generateFruits() {
    this.loses = Number(this.loses) + this.expenseValue;
    localStorage.setItem("loses", this.loses);

    this.counter++;
    localStorage.setItem("counter", this.counter);
    let random1 = Math.floor(
      Math.random() * this.fruit1.length
    );
    let random2 = Math.floor(
      Math.random() * this.fruit2.length
    );
    let random3 = Math.floor(
      Math.random() * this.fruit3.length
    );
    let random4 = Math.floor(
      Math.random() * this.fruit3.length
    );
    this.youAreWinner = false;
    this.img1 = this.fruit1[random1];
    this.img2 = this.fruit2[random2];
    this.img3 = this.fruit3[random3];
    this.img4 = this.fruit3[random4];
    if (this.img1.img == this.img2.img && 
      this.img1.img == this.img3.img 
      &&  this.img1.img == this.img4.img)  {
      this.youAreWinner = true;
      this.isWinner++;
      localStorage.setItem("isWinner", this.isWinner);
      this.earnings = Number(this.earnings) + this.winnerValue;
      localStorage.setItem("earnings", this.earnings);
    }
    this.ratioWinner = ((this.isWinner/this.counter) * 100); 
  }
  onIonKnobMoveStart(event: any){
  }
  onIonKnobMoveEnd(event: any){
    this.expenseValue = event.detail.value;
    this.winnerValue = this.expenseValue * 10;
    console.log(event.detail.value)
  }
}
