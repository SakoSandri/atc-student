import { Component } from '@angular/core';
import { UniversityService } from '../services/university.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
public data: any = [];
  constructor(public universityService: UniversityService) {}
  searchData(data: any){
    let url = `https://api.nationalize.io?name=${data.target.value}`;
    this.universityService.getData(url).subscribe((response: any)=>{
      this.data = response.country;
    })
  }

}
