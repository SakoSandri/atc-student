import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  emailText: string = '';
  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }
  emailInput(data: any) {
    console.log(data)
    this.emailText = data.target.value;
  }
  login() {
    this.router.navigateByUrl('tabs/tab1');
  }
}
