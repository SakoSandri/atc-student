import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { CardsPokemonModule } from '../components/cards-pokemon/cards-pokemon.module';
import { PokemonPageRoutingModule } from './pokemon-routing.module';

import { PokemonPage } from './pokemon.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardsPokemonModule,
    PokemonPageRoutingModule
  ],
  declarations: [PokemonPage]
})
export class PokemonPageModule {}
