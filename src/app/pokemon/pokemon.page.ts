import { Component, OnInit } from '@angular/core';
import { UniversityService } from '../services/university.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.page.html',
  styleUrls: ['./pokemon.page.scss'],
})
export class PokemonPage implements OnInit {
  public data: any = [];

  constructor(public universityService: UniversityService) { }

  ngOnInit() {
  }
  searchData(data: any){
    let url = `https://pokeapi.co/api/v2/pokemon/${data.target.value}/`;
    this.universityService.getData(url).subscribe((response: any)=>{
      this.data = response.forms;
    })
  }
}
