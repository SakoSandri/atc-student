import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { CardsPokemonComponent } from './cards-pokemon.component';

@NgModule({
  declarations: [CardsPokemonComponent],
  imports: [
    CommonModule, IonicModule
  ],
  exports: [CardsPokemonComponent]
})
export class CardsPokemonModule { }
