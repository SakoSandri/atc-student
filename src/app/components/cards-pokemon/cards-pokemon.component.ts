import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cards-pokemon',
  templateUrl: './cards-pokemon.component.html',
  styleUrls: ['./cards-pokemon.component.scss'],
})
export class CardsPokemonComponent  implements OnInit {
  @Input() data: any = [];
  constructor() { }

  ngOnInit() {}

}
