import { Component, OnInit, Input } from '@angular/core';
import { country } from 'src/environments/countryid';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent  implements OnInit {
  @Input() public data: any = [];
  public country_id: any = country;
  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }

}
