import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsComponent } from './cards.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [CardsComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    CardsComponent
  ]
})
export class CardsModule { }
